package entities;

import java.io.Serializable;
import java.util.*;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.AjaxBehaviorEvent;
/**
 *
 * @author shahk_000
 */
@ManagedBean(name="users")
@SessionScoped
public class Users implements Serializable {
    private String fname;
    private String mname;
    private String lname;
    private String faculty;
    private String program;
    private Map<String, Map<String,String>> data = new HashMap<String, Map<String, String>>();
    
    private Map<String,String> facultyList;
    private Map<String,String> programList;

    private ArrayList<Users> usersList;
    
    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getMname() {
        return mname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    public Map<String,Map<String,String>> getData() {
        return data;
    }


    public void setData(Map<String,Map<String,String>> data) {
        this.data = data;
    }
    
    public Map<String, String> getFacultyList() {
        return facultyList;
    }

    public void setFacultyList(Map<String, String> facultyList) {
        this.facultyList = facultyList;
    }

    public Map<String, String> getProgramList() {
        return programList;
    }

    public void setProgramList(Map<String, String> programList) {
        this.programList = programList;
    }

    public ArrayList<Users> getUsersList() {
        return usersList;
    }

    public void setUsersList(ArrayList<Users> usersList) {
        this.usersList = usersList;
    }
 
    public Users() {
    }
    
    @PostConstruct
    public void init(){
        usersList= new ArrayList<>();
        
        
        facultyList = new HashMap<>();
        facultyList.put("Management","Management");
        facultyList.put("Science and Techology", "Science and Technology");
        
        Map<String,String> map = new HashMap<>();
        map.put("BBA", "BBA");
        map.put("BBS", "BBS");
        data.put("Management", map);
        
        
        map = new HashMap<>();
        map.put("BE Comp", "BE Comp");
        map.put("BCA", "BCA");
        data.put("Science and Technology", map);
    }
    public void onFacultyChange(){
        if(faculty !=null && !faculty.equals(""))
            programList = data.get(faculty);
        else
            programList = new HashMap<>();            
    }
    public void save() {

        Users users = new Users();

        users.setFname(fname);
        users.setMname(mname);
        users.setLname(lname);
        users.setFaculty(faculty);
        users.setProgram(program);
        
        usersList.add(users); 
         fname=null;
         mname=null;
         lname=null;
         faculty=null;
         onFacultyChange();
         program=null;
    }
    
    public void edit(Users users) {
        
    this.fname = users.fname;
    this.mname = users.mname;
    this.lname = users.lname;
    this.faculty = users.faculty;
    onFacultyChange();
    this.program = users.program;
    
    delete(users);
      
    
    }
        
    public void delete(Users users){
    
    usersList.remove(users); 
    }
    
}
